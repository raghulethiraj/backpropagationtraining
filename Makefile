CC = g++

CFLAGS = -Wall -std=c++14

SOURCES = $(wildcard src/*.cpp)
OBJECTS = $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

SRCDIR = src
OBJDIR = obj

EXECUTABLE = BackPropagation

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

$(OBJECTS) : $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -o $@ -c $^

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)
