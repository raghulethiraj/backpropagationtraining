#include<vector>
#include<iostream>

#include "BackPropagation.hpp"
#include "NeuralNetwork.hpp"
#include "Perceptron.hpp"

using namespace std;

BackPropagation::BackPropagation(vector<int> _nnStructure, double _learnRate, double _mFactor) {
  // Initialize all the vectors
  gradients.resize(_nnStructure.size() - 1);
  prevWeightDeltas.resize(_nnStructure.size() - 1);
  prevBiasDeltas.resize(_nnStructure.size() - 1);
  
  for(int i=1; i<_nnStructure.size(); i++) {
    int numWeights = _nnStructure[i-1] * _nnStructure[i];
    int numNode = _nnStructure[i];

    prevWeightDeltas[i-1].resize(numWeights, 0.);
    prevBiasDeltas[i-1].resize(numNode, 0.);
  }

  learningRate = _learnRate;
  momentumFactor = _mFactor;
}

void BackPropagation::update(NeuralNetwork &nn, vector<double> output) {
  
  // Resize gradients to the size of all hidden + output layers
  // gradients.resize(nn.brain.size() - 1);
  /* Compute output gradients */

  // Get output layer
  vector<Perceptron> outputLayer = nn.brain[nn.brain.size() - 1];

  for(int i=0; i < outputLayer.size(); i++) {
    // desired - computed
    double diff = output[i] - outputLayer[i].getOutput();

    // Compute gradient from derivative * difference
    double gradient = outputLayer[i].derivative(outputLayer[i].getOutput()) * diff;

    // Push gradient into the output layer's vector
    gradients[gradients.size() - 1].push_back(gradient);
  }

  /* Compute hidden layer gradients */

  // Iterate upto last hidden layer
  for(int i=nn.brain.size() - 2; i > 0; i--) {

    // Iterate through all the nodes in the hidden layer
    for(int j=0; j < nn.brain[i].size(); j++) {
      // Get the derivaive of the j-th node
      double computedOutput = nn.brain[i][j].getOutput();
      double derivative = nn.brain[i][j].derivative(computedOutput);
      
      double gradient = derivative * weightsGradientSum(nn.brain[i + 1], j, i);

      // Save the node's gradient
      gradients[i - 1].push_back(gradient);
    }
  }

  /* Updating weights & biases*/

  // Starting from last hidden to input layer
   for(int i=nn.brain.size() - 2; i >= 0; i--) {
    // Get the down stream layer
    vector<Perceptron> &downStreamLayer = nn.brain[i+1];
    
    // Going through all the nodes in this layer
    for(int j=0; j < nn.brain[i].size(); j++) {

      // If input layer
      if(i == 0) {
	
	// Going through all the nodes in the downstream layer
	for(int k=0; k < downStreamLayer.size(); k++) {
	
	  double upStreamOutput = nn.brain[i][j].getOutput();
	  double downStreamGradient = gradients[i][k];

	  double wMomentum = momentumFactor * prevWeightDeltas[i][j + (nn.brain[i].size()*k)];
	  double wDelta = learningRate * upStreamOutput * downStreamGradient;

	  // Store the previous delta
	  prevWeightDeltas[i][j + (nn.brain[i].size()*k)] = wDelta;

	  // Compute the new weight
	  double newWeight = downStreamLayer[k].getWeight(j) + wDelta + wMomentum;

	  // Update the weight connecting the hidden node and
	  // corresponding downstream layer's node
	  downStreamLayer[k].setWeight(j, newWeight);
	}

      } else {

	/* Updating the bias for this node*/
	double bDelta = learningRate * gradients[i-1][j];
	double bMomentum = learningRate * prevBiasDeltas[i-1][j];

	double newBias = nn.brain[i][j].getBias() + bDelta + bMomentum;
	nn.brain[i][j].setBias(newBias);
	
	// Going through all the nodes in the downstream layer
	for(int k=0; k < downStreamLayer.size(); k++) {
	
	  double upStreamOutput = nn.brain[i][j].getOutput();
	  double downStreamGradient = gradients[i][k];

	  double wMomentum = momentumFactor * prevWeightDeltas[i][j + (nn.brain[i].size()*k)];
	  double wDelta = learningRate * upStreamOutput * downStreamGradient;

	  // Store the previous delta
	  prevWeightDeltas[i][j + (nn.brain[i].size()*k)] = wDelta;

	  // Compute the new weight
	  double newWeight = downStreamLayer[k].getWeight(j) + wDelta + wMomentum;

	  // Update the weight connecting the hidden node and
	  // corresponding downstream layer's node
	  downStreamLayer[k].setWeight(j, newWeight);
	}
      }
    }
  }
}

double BackPropagation::weightsGradientSum(vector<Perceptron> &gradientNodes, unsigned int node, unsigned int gradientLayer) {

  double sum = 0.;
  
  for(int i=0; i < gradientNodes.size(); i++) {
    sum += gradientNodes[i].getWeight(node) * gradients[gradientLayer][i];
  }
}
