#include <iostream>
#include <vector>
#include <math.h>

#include "NeuralNetwork.hpp"
#include "BackPropagation.hpp"

using namespace std;

void printVector(vector<double> values) {
  for (int i = 0; i < values.size(); i++) {
    cout << values[i] << endl;
  }
}

int main(int argc, char *argv[]) {

  vector<int> nnStructure = {2, 3, 2};
  double learningRate = 1.;
  double momentumFactor = 0.7;
  
  NeuralNetwork nn(nnStructure);
  
  BackPropagation trainer(nnStructure, learningRate, momentumFactor);

  int maxEpoch = 3;

  cout << "Before training" << endl;

  cout << "Expected Output" << endl;
  cout << 0.45 << endl << 0.55 << endl;

  cout << "Untrained Prediction" << endl;
  printVector(nn.predict({100,10}));
	      
  while(maxEpoch > 0) {
    vector<double> computedOutput = nn.predict({100, 10});

    double accuracy = fabs(computedOutput[0] - 0.45) + fabs(computedOutput[1] - 0.55);

    if(accuracy < 0.005) {
      cout << "It took " << maxEpoch << " epochs to train this NN" << endl;
      cout << "Trained Prediction" << endl;
      printVector(computedOutput);
      
      break;
    } else {
      trainer.update(nn, {0.45, 0.55});
      cout << "Epoch : " << maxEpoch << endl;
      
      nn.visualizeBrain();
      
      maxEpoch--;
    }
  }

  cout << "Partially Trained Prediction" << endl;
  printVector(nn.predict({100,10}));
  
  return 0;
}

