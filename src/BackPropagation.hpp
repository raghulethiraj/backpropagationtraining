#pragma once

#include<vector>

#include "NeuralNetwork.hpp"
#include "Perceptron.hpp"

class BackPropagation
{
public:
  BackPropagation(std::vector<int>, double, double);
  
  // Takes a NN and it's expected output and updates values
  void update(NeuralNetwork&, std::vector<double>);

private:
  // Stores gradients from hidden to output layer;
  // gradients[0] is 1st hidden layer from the input layer
  std::vector<std::vector<double>> gradients;
  std::vector<std::vector<double>> prevWeightDeltas;
  std::vector<std::vector<double>> prevBiasDeltas;

  double weightsGradientSum(std::vector<Perceptron>&, unsigned int, unsigned int);
  double learningRate;
  double momentumFactor;

};
